#FROM tomcat:9.0-jdk8
FROM tomcat:9.0-jdk8-adoptopenjdk-openj9
LABEL authors="dimitrios.chalepakis@pta.de, simon.reis@pta.de" 

COPY deploy/conf/contextExternalConfig.xml /usr/local/tomcat/conf/context.xml
COPY deploy/conf/web.xml /usr/local/tomcat/conf/web.xml

RUN mkdir /usr/local/tomcat/webapps/ROOT
COPY deploy/conf/index.jsp /usr/local/tomcat/webapps/ROOT/index.jsp

COPY portal.war /usr/local/tomcat/webapps/portal.war

COPY portalFE/ /usr/local/tomcat/webapps/portalFE/
