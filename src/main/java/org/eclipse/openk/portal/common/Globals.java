/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;


public final class Globals {
    public static final String OK_STRING = "OK";
    public static final String SESSION_TOKEN_TAG = "X-XSRF-TOKEN";
    public static final String SESSION_COOKIE_TOKEN_TAG = "COOKIESESSION-TOKEN";
    public static final int MAX_CREDENTIALS_LENGTH = 500;
    public static final int MAX_NOTIFICATION_LENGTH = 16384;

    public static final int HTTPSTATUS_OK = 200;
    public static final int HTTPSTATUS_BAD_REQUEST = 400;
    public static final int HTTPSTATUS_UNAUTHORIZED = 401;
    public static final int HTTPSTATUS_FORBIDDEN = 403;
    public static final int HTTPSTATUS_NOT_FOUND = 404;
    public static final int HTTPSTATUS_GONE = 410;
    public static final int HTTPSTATUS_LOCKED = 423;
    public static final int HTTPSTATUS_INTERNAL_SERVER_ERROR = 500;
    public static final int HTTPSTATUS_NOT_AVAILABLE = 503;
    public static final int HTTPSTATUS_POLICY_NOT_FULFILLED = 420;
    public static final int HTTPSTATUS_CONFLICT = 409;
    public static final int HTTPSTATUS_DECISION = 900;

    //For Branches: for later use in a configurable json
    public static final String ELECTRICITY_MARK = "S";
    public static final String GAS_MARK = "G";
    public static final String DISTRICT_HEAT_MARK = "F";
    public static final String WATER_MARK = "W";

    //Errormessage
    public static final String DATA_OUTDATED = "Data is outdated!";

    //Suggestion: An input field on search mask as better solution? Can only be adapted by code changes.
    public static final int FAST_SEARCH_NUMBER_OF_DAYS_BACK = -200;

    //KeyCloak configuration
    public static final String KEYCLOAK_AUTH_TAG = "Authorization";

    private Globals() {}

}
