/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common.util;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.net.SyslogAppender;
import org.apache.log4j.spi.RootLogger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * <b>LoggerConfig</b><br>
 * Logging Implementierung
 */
public final class LoggerUtil extends HttpServlet {
    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
    public void init() {
        final ServletContext context = getServletContext();

        if (Boolean.valueOf(context.getInitParameter("param.syslog.use"))) {
            // configure for syslog using parameters specified in configuration descriptor
            BasicConfigurator.resetConfiguration();

            final String level = context.getInitParameter("param.syslog.level");
            RootLogger.getRootLogger().setLevel(Level.toLevel(level));

            final String host = context.getInitParameter("param.syslog.host");
            final String facility = context.getInitParameter("param.syslog.facility");
            final Layout layout = new PatternLayout("[%d{yyyy.MM.dd HH:mm:ss}] [%p] [%c] %m%n");
            final Appender syslogAppender = new SyslogAppender(layout, host,
                    SyslogAppender.getFacility(facility));
            BasicConfigurator.configure(syslogAppender);

        } else {
            // configure using parameters specified in xml file
            final String prefix = context.getRealPath("/");
            final String file = getInitParameter("log4j-init-file");

            if (prefix != null && file != null) {
                DOMConfigurator.configure(prefix + file);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        // Nothing done on purpose.
    }
}
