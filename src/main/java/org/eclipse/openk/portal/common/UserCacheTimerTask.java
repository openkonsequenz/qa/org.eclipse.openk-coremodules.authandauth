/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import org.apache.log4j.Logger;
import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.auth2.model.KeyCloakRole;
import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import org.eclipse.openk.portal.auth2.util.JwtHelper;
import org.eclipse.openk.portal.controller.TokenManager;
import org.eclipse.openk.portal.exceptions.PortalInternalServerError;
import org.eclipse.openk.portal.viewmodel.UserCache;

public class UserCacheTimerTask extends TimerTask {

  private static final Logger LOGGER = Logger.getLogger(TokenManager.class.getName());

  @Override
  public void run() {
    LOGGER.debug("UserCacheTimerTask started");
    try {
      JwtToken token = JwtHelper.login(BackendConfig.getInstance().getKeycloakAdmin(), BackendConfig.getInstance().getKeycloakPW());
      if( token == null ) {
        LOGGER.error("Login for getKeyCloakUsersFromToken() failed for User:"+ BackendConfig.getInstance().getKeycloakAdmin());
        return;
      }

      List<KeyCloakUser> allUsersWithRoles = getKeyCloakUsersFromToken(token);

      if( allUsersWithRoles.isEmpty()) {
        LOGGER.warn("getKeyCloakUsersFromToken provided empty list!");
      }

      UserCache.getInstance().setKeyCloakUsers(allUsersWithRoles);

    } catch (PortalInternalServerError portalInternalServerError) {
      LOGGER.error(portalInternalServerError);
    }

    LOGGER.debug("UserCacheTimerTask finished");
  }

  private List<KeyCloakUser> getKeyCloakUsersFromToken(JwtToken token) throws PortalInternalServerError {
    List<KeyCloakUser> allUsers = JwtHelper.getUsers(token, BackendConfig.getInstance().getMaxLoadUsers());
    List<KeyCloakUser> allUsersWithRoles = new ArrayList<>();

    for (KeyCloakUser user : allUsers) {
      List<String> rolesList = new ArrayList<>();
      List<KeyCloakRole> rolesForUser = JwtHelper.getRolesForUser(token, user.getId());
      user.setName();
      for (KeyCloakRole keyCloakRole : rolesForUser) {
        rolesList.add(keyCloakRole.getName());
      }
      user.setAllRoles(rolesList);
      allUsersWithRoles.add(user);
    }
    return allUsersWithRoles;
  }
}
