/*
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.health.base;


public class Result {
    private boolean healthy;
    private String message;

    private Result() {}

    public static Result healthy() {
        Result res = new Result();
        res.healthy = true;
        return res;
    }

    public static Result unhealthy(String message) {
        Result res = new Result();
        res.healthy = false;
        res.message = message;
        return res;
    }

    public String getMessage() {
        return message;
    }

    public boolean isHealthy() {
        return healthy;
    }
}
