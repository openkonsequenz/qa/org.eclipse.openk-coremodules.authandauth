/*
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
*/
package org.eclipse.openk.portal.health.base;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HealthChecker {
    private static final Logger logger = Logger.getLogger(HealthChecker.class);

    private List<HealthCheck> registeredHealthChecks = new LinkedList<>();
    private Map<HealthCheck, String> namingMap = new HashMap<>();

    public void register( String title, HealthCheck hc ) {
        namingMap.put(hc, title);
        registeredHealthChecks.add(hc);
    }

    public List<NamedHealthCheckResult> performHealthChecks() {
        List<NamedHealthCheckResult> hcList = new LinkedList<>();
        for( HealthCheck hc : registeredHealthChecks) {
            try {
                hcList.add(new NamedHealthCheckResult(namingMap.get(hc), hc.check()));

            } catch (Throwable t) { // NOSONAR  we definately want to catch everything here!!!
                logger.error("Error during healthcheck", t);
                hcList.add(new NamedHealthCheckResult(namingMap.get(hc),
                        Result.unhealthy("Exception during test: "+t+" -> See Log!")));
            }
        }
        return hcList;
    }

}
