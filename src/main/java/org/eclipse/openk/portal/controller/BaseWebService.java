/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.exceptions.PortalExceptionMapper;

public abstract class BaseWebService {
    private final Map<String, Long> currentTimeMeasures = new HashMap<>();
    private final Logger logger;

    public BaseWebService(Logger logger) {
        this.logger = logger;
    }

    protected abstract void assertAndRefreshToken(String token, boolean refresh) throws PortalException;

    private void startProcessing(String methodName) {
        Long lStartingTime = System.currentTimeMillis();
        String lookupName = buildLookupId(methodName);
        currentTimeMeasures.put(lookupName, lStartingTime);

        logger.trace(this.getClass().getName() + "." + lookupName + ": Start processing...");
    }

    protected AutoCloseable perform(final String methodName) {
        startProcessing(methodName);
        return () -> endProcessing(methodName);
    }

    private String buildLookupId(String func) {
        return func + "@@" + Thread.currentThread().getId();
    }

    private void endProcessing(String methodName) {
        String lookupName = buildLookupId(methodName);
        if (currentTimeMeasures.containsKey(lookupName)) {
            Long lStartingTime = currentTimeMeasures.get(lookupName);
            currentTimeMeasures.remove(lookupName);

            logger.trace(this.getClass().getName() + "." + lookupName + ": Finished processing in " + (System.currentTimeMillis() - lStartingTime) + " ms");
        }

    }

    protected Response invoke(String sessionId, boolean refresh, BackendInvokable invokable)
    {
        try (AutoCloseable ignored = perform(invokable.getClass().getName() + ".Invoke()")) { // NOSONAR

            assertAndRefreshToken(sessionId, refresh);

            return invokable.invoke();
        } catch (PortalException bee) {
            logger.debug("Caught BackendException: " + bee.getClass().getSimpleName());
            return Response.status(bee.getHttpStatus()).entity(PortalExceptionMapper.toJson(bee))
                    .build();

        } catch (Exception e) {
            logger.error("Unexpected exception", e);
            return Response.status(Globals.HTTPSTATUS_INTERNAL_SERVER_ERROR).build();
        }

    }
}
