/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;


import org.eclipse.openk.portal.auth2.model.JwtPayload;
import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.auth2.util.JwtHelper;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.eclipse.openk.portal.common.BackendConfig;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.eclipse.openk.portal.viewmodel.UserAuthentication;

public class TokenManager {
    private static final Logger LOGGER = Logger.getLogger(TokenManager.class.getName());

    private static long internalSessionLengthOffsetPerc = 0L;

    public static class SessionItem {
        private long sessionCreationTime;
        private long sessionLastAccessTime;

        private String sessionId;
        private String cookieToken;
        private UserAuthentication user;
        private JwtToken jwtToken;

        public SessionItem(long sessionCreationTime) {
            this.sessionCreationTime = this.sessionLastAccessTime = sessionCreationTime;
        }

        public long getSessionCreationTime() {
            return sessionCreationTime;
        }

        public void setSessionCreationTime(long sessionCreationTime) {
            this.sessionCreationTime = sessionCreationTime;
        }

        public long getSessionLastAccessTime() {
            return sessionLastAccessTime;
        }

        public void setSessionLastAccessTime(long sessionLastAccessTime) {
            this.sessionLastAccessTime = sessionLastAccessTime;
        }

        public UserAuthentication getUser() {
            return user;
        }

        public void setUser(UserAuthentication user) {
            this.user = user;
        }

        public JwtToken getJwtToken() {
            return jwtToken;
        }

        public void setJwtToken(JwtToken jwtToken) {
            this.jwtToken = jwtToken;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getCookieToken() {
            return cookieToken;
        }

        public void setCookieToken(String cookieToken) {
            this.cookieToken = cookieToken;
        }
    }

    private static final TokenManager INSTANCE = new TokenManager();
    private final HashMap<String, SessionItem> registry = new HashMap<>();

    private TokenManager() {
    }

    public static TokenManager getInstance() {
        return INSTANCE;
    }

    public void registerNewSession(JwtToken jwtToken) {

        String guid = jwtToken.getAccessToken();
        SessionItem sitem = new SessionItem(System.currentTimeMillis());
        sitem.setJwtToken(jwtToken);
        sitem.setSessionId(guid);

        LOGGER.debug("Registered Session with JWT: " + JsonGeneratorBase.getGson().toJson(jwtToken));
        registry.put(guid, sitem);
    }


    private boolean sessionHasExpired(long now, SessionItem sitem) {
        return (now - sitem.getSessionLastAccessTime()) >
                BackendConfig.getInstance().getInternalSessionLengthMillis() * (1 + (internalSessionLengthOffsetPerc / 100L));
    }

    public void refreshSessionIsAlive(String token, boolean refresh) throws PortalUnauthorized {
        String accesstoken = JwtHelper.formatToken(token);
        SessionItem sitem = registry.get(accesstoken);
        long now = System.currentTimeMillis();

        if (sitem == null || sessionHasExpired(now, sitem)) {
            if (sitem != null) {
                JwtPayload payLoad = JwtHelper.getPayLoad(sitem.getJwtToken());
                String user = (payLoad != null ? payLoad.getPreferredUsername() : null);
                LOGGER.debug("Session expired for User " + user);
            }
            registry.remove(token); // we're done
            LOGGER.debug("Invalid Token: " + token);
            throw new PortalUnauthorized("Invalid Token");
        }

        if (refresh) {
            sitem.setSessionLastAccessTime(now);
        }
    }

    public void logout(String token) {
        String accesstoken = JwtHelper.formatToken(token);
        if (registry.containsKey(accesstoken)) {
            registry.remove(accesstoken);
        }
    }

}
