/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/

package org.eclipse.openk.portal.controller;

import org.apache.log4j.Logger;
import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import org.eclipse.openk.portal.auth2.util.JwtHelper;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.eclipse.openk.portal.viewmodel.LoginCredentials;
import org.eclipse.openk.portal.viewmodel.UserCache;
import org.eclipse.openk.portal.viewmodel.UserModule;
import org.eclipse.openk.portal.viewmodel.VersionInfo;

import java.util.ArrayList;
import java.util.List;

public class BackendController {
    private static final Logger LOGGER = Logger.getLogger(BackendController.class.getName());
    private final InputDataValuator inputDataValuator = new InputDataValuator();

    public List<KeyCloakUser> getUsersForRole(String roleToCheck) {
        List<KeyCloakUser> usersWithRole = new ArrayList<>();
        List<KeyCloakUser> keyCloakUsers = UserCache.getInstance().getKeyCloakUsers();

        for (KeyCloakUser user : keyCloakUsers) {
            List<String> allRoles = user.getAllRoles();
            if (allRoles ==  null) {
                continue;
            }

            for (String role : allRoles) {
                if(role.equals(roleToCheck)){
                    usersWithRole.add(user);
                    break;
                }
            }
        }

        return usersWithRole;
    }


    public JwtToken authenticate(String credentials) throws PortalException {
        LOGGER.debug("authenticate() is called");

        // valuator will throw an exception if not valid
        inputDataValuator.checkCredentials(credentials);

        LoginCredentials loginCredentials = JsonGeneratorBase.getGson().fromJson(credentials, LoginCredentials.class);

        JwtToken token = null;
        String inputUsername = loginCredentials.getUserName().toLowerCase();
        String inputPassword = loginCredentials.getPassword();

        // obtain a token from keycloak
        token = JwtHelper.login(inputUsername, inputPassword);

        if( token == null ) {
            LOGGER.debug(credentials);
            throw new PortalUnauthorized("Unknown User/Password");
        }

        LOGGER.debug("authenticate() succeeded.");
        return token;
    }

    public VersionInfo getVersionInfo() {
        LOGGER.debug("getVersionInfo() is called");

        String version = getClass().getPackage().getImplementationVersion();
        VersionInfo vi = new VersionInfo();
        vi.setBackendVersion(version);

        LOGGER.debug("getVersionInfo() is finished");
        return vi;
    }

    public List<UserModule> getUserModuleList() {
        LOGGER.debug("getUserModuleList() is called");
        List<UserModule> umList = new ArrayList<>();

        for (int i=0; i<UserModule.getInstance().length; i++)
        {
            UserModule um = new UserModule();
            um.setModuleName(UserModule.getInstance()[i].getModuleName());
            um.setCols(UserModule.getInstance()[i].getCols());
            um.setRows(UserModule.getInstance()[i].getRows());
            um.setColor(UserModule.getInstance()[i].getColor());
            um.setLink(UserModule.getInstance()[i].getLink());
            um.setPictureLink(UserModule.getInstance()[i].getPictureLink());
            um.setRequiredRole(UserModule.getInstance()[i].getRequiredRole());

            umList.add(um);
        }

        LOGGER.debug("getUserModuleList() is finished");
        return umList;
    }

}
