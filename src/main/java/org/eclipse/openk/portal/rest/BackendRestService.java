/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.rest;

import javax.ws.rs.*;

import org.apache.log4j.Logger;
import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.controller.BackendController;
import org.eclipse.openk.portal.controller.BaseWebService;
import org.eclipse.openk.portal.controller.ControllerImplementations;
import org.eclipse.openk.portal.controller.ResponseBuilderWrapper;
import org.eclipse.openk.portal.controller.TokenManager;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.exceptions.PortalExceptionMapper;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.eclipse.openk.portal.health.base.HealthChecker;
import org.eclipse.openk.portal.health.base.NamedHealthCheckResult;
import org.eclipse.openk.portal.health.impl.KeyCloakPresentHealthCheck;
import org.eclipse.openk.portal.viewmodel.VersionInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/beservice")
public class BackendRestService extends BaseWebService {
    private static final Logger logger = Logger.getLogger(BackendRestService.class.getName());
    private static final boolean DEVELOP_MODE;
    private static final String LET_ME_IN = "LET_ME_IN";

    static {
        // determine static VersionInfo
        String versionString = BackendRestService.class.getPackage().getImplementationVersion().toUpperCase();
        DEVELOP_MODE = versionString.contains("DEVELOP") || versionString.contains("SNAPSHOT");
    }
    @Context
    private UriInfo uriInfo;

    public BackendRestService() {
        super(logger);
    }

    @POST
    @Path("/login")
    @Produces("application/json")
    @Consumes("application/json")
    public Response login(String loginCredentials) {

        try (AutoCloseable ignored = perform("login()")) { // NOSONAR
            JwtToken jwtToken;

            if (loginCredentials == null || loginCredentials.isEmpty()) {
                logger.info("Login Error. Empty Credentials");
                throw new PortalUnauthorized();
            } else {
                // will throw exception if it fails
                jwtToken = new BackendController().authenticate(loginCredentials);
                TokenManager tokenManager = TokenManager.getInstance();
                tokenManager.registerNewSession(jwtToken);
                logger.debug("Login Success");
            }

            return createJsonResponse(jwtToken);
        } catch (Exception e) {
            logger.info("Login Error. Invalid KeyCloak Credentials/Settings");
            return responseFromException(e);
        }
    }

    private Response createJsonResponse(Object obj) throws PortalException {
        Response.ResponseBuilder rb = ResponseBuilderWrapper.INSTANCE
            .getResponseBuilder(obj != null ? JsonGeneratorBase.getGson().toJson(obj) : PortalExceptionMapper.getGeneralOKJson());

        return rb.build();
    }

    @GET
    @Path("/checkAuth")
    @Produces("application/json")
    public Response checkAuth(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String accessToken) {
        logger.trace("checkAuth portal called");
        return invoke(accessToken, true, new ControllerImplementations.CheckAuth());
    }

    @GET
    @Path("/logout")
    @Produces("application/json")
    public Response logout(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String accessToken) {
        return invoke(accessToken, false, new ControllerImplementations.Logout(accessToken));
    }

    @GET
    @Path("/versionInfo/")
    @Produces("application/json")
    public Response getVersionInfo() {
        try (AutoCloseable ignored = perform("getVersionInfo()")) { // NOSONAR
            VersionInfo versionInfo = new BackendController().getVersionInfo();
            return createJsonResponse(versionInfo);
        } catch (Exception e) {
            return responseFromException(e);
        }
    }

    @GET
    @Path("/userModulesForUser/")
    @Produces("application/json")
    public Response getUserModulesForUser(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String accessToken) {
        return invoke(accessToken, false, new ControllerImplementations.GetUserModulesForUser(new BackendController()));
    }

    @GET
    @Path("/usersForRole/{userRole}")
    @Produces("application/json")
    public Response getUsersForRole(@PathParam("userRole") String userRole,
        @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String accessToken) {
        return invoke(accessToken, false, new ControllerImplementations.GetUsersForRole(new BackendController(),userRole));
    }

    @GET
    @Path("/users")
    @Produces("application/json")
    public Response getUsers(@HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String accessToken) {
        return invoke(accessToken, false, new ControllerImplementations.GetUsers());
    }

    @GET
    @Path("/healthcheck")
    @Produces("application/json")
    public Response getHealthCheck() {
        HealthChecker hc = new HealthChecker();
        hc.register("Keycloak present", new KeyCloakPresentHealthCheck());

        String returnJson = NamedHealthCheckResult.toJson(hc.performHealthChecks());

        try {
            return ResponseBuilderWrapper.INSTANCE.buildOKResponse(returnJson);
        } catch (PortalException e) {
            logger.error("unexpected error", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private boolean isBackdoor( String sessionId ) {
        // backdoor is only available when the version(POM) contains "DEVELOP" or "SNAPSHOT"
        return DEVELOP_MODE && LET_ME_IN.equals(sessionId);
    }

    @Override
    protected void assertAndRefreshToken(String token, boolean refresh) throws PortalUnauthorized {
        if (isBackdoor(token)) {
            return;
        }
        TokenManager.getInstance().refreshSessionIsAlive(token, refresh);

    }

    private Response responseFromException(Exception e) {
        int errcode;
        String retJson;

        if (e instanceof PortalException) {
            logger.error("Caught BackendException", e);
            errcode = ((PortalException) e).getHttpStatus();
            retJson = PortalExceptionMapper.toJson((PortalException) e);
            return Response.status(errcode).entity(retJson).build();
        } else {
            logger.error("Unexpected exception", e);
            return Response.status(Globals.HTTPSTATUS_INTERNAL_SERVER_ERROR).entity(PortalExceptionMapper.getGeneralErrorJson()).build();
        }
    }

}
