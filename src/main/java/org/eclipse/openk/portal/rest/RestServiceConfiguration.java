/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.rest;


import java.util.HashSet;
import java.util.Set;

public class RestServiceConfiguration extends javax.ws.rs.core.Application {
    public RestServiceConfiguration() {
        // Standard Contstructor needed
    }

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> restServicesClasses = new HashSet<>();
        restServicesClasses.add(BackendRestService.class);
        return restServicesClasses;
    }
}
