/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

public class JwtHeader {
    private String alg;
    private String typ;
    private String kid;

    public String getAlg() { return alg; }
    public void setAlg(String alg) { this.alg = alg; }
    public String getTyp() { return typ; }
    public void setTyp(String typ) { this.typ = typ; }
    public String getKid() { return kid; }
    public void setKid(String kid) { this.kid = kid; }
}
