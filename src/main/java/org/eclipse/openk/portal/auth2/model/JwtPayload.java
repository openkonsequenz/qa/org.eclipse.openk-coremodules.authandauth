/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;


public class JwtPayload {

    //No Sonar because variables are wrong due to jwt definition

    private String jti;
    private long exp;
    private long nbf;
    private long iat;
    private String iss;

    //Since Keycloak 4.8.3 "aud" Parameter is an array now
    //Pre 4.8.3 "aud" was a single String
    //We keep it for documentation reasons
    //private String[] aud; //NOSONAR
    //private String aud; //NOSONAR
    private String sub;
    private String typ;
    private String azp;
    private String nonce;
    private Integer auth_time; //NOSONAR
    private String session_state; //NOSONAR
    private String acr;

    @JsonProperty("allowed-origins")
    private List<String> allowedOrigins;
    private JwtRealmAccess realm_access; //NOSONAR
    private JwtResourceAccess resource_access; //NOSONAR

    private String name;
    private String preferred_username; //NOSONAR
    private String given_name; //NOSONAR
    private String family_name; //NOSONAR

    public String getJti() { return jti; }
    public void setJti(String jti) { this.jti = jti; }

    public long getExp() { return exp; }
    public void setExp(long exp) { this.exp = exp; }

    public long getNbf() { return nbf; }
    public void setNbf(long nbf) { this.nbf = nbf; }

    public long getIat() { return iat; }
    public void setIat(long iat) { this.iat = iat; }

    public String getiss() { return iss; }
    public void setiss(String iss) { this.iss = iss; }

    public String getsub() { return sub; }
    public void setsub(String sub) { this.sub = sub; }

    public String gettyp() { return typ; }
    public void settyp(String typ) { this.typ = typ; }

    public String getAzp() { return azp; }
    public void setAzp(String azp) { this.azp = azp; }

    public String getNonce() { return nonce; }
    public void setNonce(String nonce) { this.nonce = nonce; }

    public Integer getAuthTime() { return auth_time; }
    public void setAuthTime(Integer auth_time) { this.auth_time = auth_time; } //NOSONAR

    public String getSessionState() { return session_state; }
    public void setSessionState(String session_state) { this.session_state = session_state; } //NOSONAR

    public String getAcr() { return acr; }
    public void setAcr(String acr) { this.acr = acr; }

    public List<String> getAllowedOrigins() {
        return allowedOrigins;
    }
    public void setAllowedOrigins(List<String> allowedOrigins) { this.allowedOrigins = allowedOrigins; }

    public JwtRealmAccess getRealmAccess() { return realm_access; }
    public void setRealmAccess(JwtRealmAccess realm_access) { this.realm_access = realm_access; } //NOSONAR

    public JwtResourceAccess getResourceAccess() { return resource_access; }
    public void setResourceAccess(JwtResourceAccess resource_access) { this.resource_access = resource_access; } //NOSONAR

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getPreferredUsername() { return preferred_username; }
    public void setPreferredUsername(String preferred_username) { this.preferred_username = preferred_username; } //NOSONAR

    public String getGivenName() { return given_name; }
    public void setGivenName(String given_name) { this.given_name = given_name; } //NOSONAR

    public String getFamilyName() { return family_name; }
    public void setFamilyName(String family_name) { this.family_name = family_name; } //NOSONAR

    public boolean isInRole(String role) {
        return this.realm_access != null && this.realm_access.isInRole(role);
    }
}
