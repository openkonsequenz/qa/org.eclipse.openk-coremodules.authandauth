/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

import java.util.List;

public class KeyCloakClientRoles {

  private String id;
  private String client;
  private List<KeyCloakRole> mappings;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public List<KeyCloakRole> getMappings() {
    return mappings;
  }

  public void setMappings(List<KeyCloakRole> mappings) {
    this.mappings = mappings;
  }
}
