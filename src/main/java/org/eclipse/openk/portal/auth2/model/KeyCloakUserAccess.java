/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

public class KeyCloakUserAccess {
    private boolean manageGroupMembership;
    private boolean view;
    private boolean mapRoles;
    private boolean impersonate;
    private boolean manage;

    public boolean getManageGroupMembership() { return manageGroupMembership; }
    public void setManageGroupMembership(boolean manageGroupMembership) { this.manageGroupMembership = manageGroupMembership; }
    public boolean getView() { return view; }
    public void setView(boolean view) { this.view = view; }
    public boolean getMapRoles() { return mapRoles; }
    public void setMapRoles(boolean mapRoles) { this.mapRoles = mapRoles; }
    public boolean getImpersonate() { return impersonate; }
    public void setImpersonate(boolean impersonate) { this.impersonate = impersonate; }
    public boolean getManage() { return manage; }
    public void setManage(boolean manage) { this.manage = manage; }
}
