/*
 ******************************************************************************
 * Copyright © 2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
*/
package org.eclipse.openk.portal.health.base;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class HealthCheckerTest {
    public boolean hc1Called;
    public boolean hc2Called;
    public boolean hc3Called;

    @Before
    public void init() {
        hc1Called = false;
        hc2Called = false;
        hc3Called = false;
    }

    @Test
    public void test_ok() {
        HealthCheck hc1 = new HealthCheck() {

            @Override
            protected Result check() throws Exception {
                hc1Called = true;
                return Result.healthy();
            }
        };

        HealthCheck hc2 = new HealthCheck() {
            @Override
            protected Result check() throws Exception {
                hc2Called = true;
                return Result.unhealthy("because of...");
            }
        };

        HealthChecker theChecker = new HealthChecker();
        theChecker.register("firstTest", hc1 );
        theChecker.register( "secondTest", hc2 );

        List<NamedHealthCheckResult> retList = theChecker.performHealthChecks();

        assertEquals( 2, retList.size() );
        assertTrue( hc1Called );
        assertTrue( hc2Called );
        assertNull( retList.get(0).getResult().getMessage());
        assertNotNull( retList.get(1).getResult().getMessage());
        assertEquals( "firstTest", retList.get(0).getName());
        assertEquals( "secondTest", retList.get(1).getName());
        assertTrue( retList.get(0).getResult().isHealthy());
        assertFalse( retList.get(1).getResult().isHealthy());
    }

    @Test
    public void test_nok() {
        HealthCheck hc1 = new HealthCheck() {

            @Override
            protected Result check() throws Exception {
                hc3Called = true;
                throw new Exception("Error in healthCheck");
            }
        };


        HealthChecker theChecker = new HealthChecker();
        theChecker.register("errorTest", hc1 );

        List<NamedHealthCheckResult> retList = theChecker.performHealthChecks();

        assertEquals( 1, retList.size() );
        assertTrue( hc3Called );
        assertNotNull( retList.get(0).getResult().getMessage());
        assertFalse( retList.get(0).getResult().isHealthy());

    }
}
