/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
package org.eclipse.openk.portal.health.base;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResultTest {

    @Test
    public void testAll() {
        Result res1 = Result.healthy();
        Result res2 = Result.unhealthy("Msg");

        assertTrue(res1.isHealthy());
        assertFalse(res2.isHealthy());
        assertNull(res1.getMessage());
        assertNotNull(res2.getMessage());

    }
}
