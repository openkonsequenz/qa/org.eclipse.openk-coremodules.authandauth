/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BackendConfigTest {
    @Test
    public void testConfig() {
        BackendConfig bc = BackendConfig.getInstance();
        assertEquals( 3600000, (int)bc.getInternalSessionLengthMillis() );
        assertEquals( 300 , (int)bc.getReloadUsersInSec());
        assertEquals( "http://entopkon:8080/", bc.getAuthServerUrl() );
        assertEquals( "Elogbook", bc.getKeycloakRealm());
        assertEquals( "elogbook-backend", bc.getKeycloakClient());
        assertEquals( "admin", bc.getKeycloakAdmin() );
        assertEquals( "admin", bc.getKeycloakPW() );
        assertEquals( "backendConfigDevLocal.json", BackendConfig.getConfigFileName() );
        BackendConfig.setConfigFileName("backendConfigProduction.json");
        assertEquals( "backendConfigProduction.json", BackendConfig.getConfigFileName() );
    }
}
