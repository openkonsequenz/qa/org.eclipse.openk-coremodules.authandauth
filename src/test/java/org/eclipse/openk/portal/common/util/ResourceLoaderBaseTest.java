/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common.util;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ResourceLoaderBaseTest {
    @Test
    public void testloadStringFromResourceError() {
        ResourceLoaderBase rlb = new ResourceLoaderBase();
        String str = rlb.loadStringFromResource("UNKNOWN_FILE");
        assertEquals("", str);
    }
}
