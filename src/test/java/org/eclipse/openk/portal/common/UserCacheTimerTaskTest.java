/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.easymock.EasyMock.expect;
import static org.eclipse.openk.portal.common.JsonGeneratorBase.getGson;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.auth2.model.KeyCloakRole;
import org.eclipse.openk.portal.auth2.model.KeyCloakRoles;
import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import org.eclipse.openk.portal.auth2.util.JwtHelper;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.exceptions.PortalInternalServerError;
import org.eclipse.openk.portal.viewmodel.UserCache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JwtHelper.class)
public class UserCacheTimerTaskTest  extends ResourceLoaderBase {

    private UserCacheTimerTask task;

    @Before
    public void prepareTests() {
        task = new UserCacheTimerTask();
    }

    @Test
    public void testGetKeyCloakUsersFromToken() throws Exception {
        String json = super.loadStringFromResource("JWTAdmin.json");
        JwtToken jwt = getGson().fromJson(json, JwtToken.class);

        String jsonKeyCloakUser = super.loadStringFromResource("testKeyCloakUsers.json");
        Type listTypeKeyCloakUser = new TypeToken<List<KeyCloakUser>>(){}.getType();
        List<KeyCloakUser> keyCloakUsersList = getGson().fromJson(jsonKeyCloakUser, listTypeKeyCloakUser);
        keyCloakUsersList.remove(1);
        keyCloakUsersList.remove(1);

        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakClientAndRealmRoles.json");
        List<KeyCloakRole> keyCloakRolesList = JwtHelper.getRolesListFromJson(jsonKeyCloakRoles);

        PowerMock.mockStatic(JwtHelper.class);
        expect(JwtHelper.login(BackendConfig.getInstance().getKeycloakAdmin(), BackendConfig.getInstance().getKeycloakPW())).andReturn(jwt);
        expect(JwtHelper.getUsers(jwt, BackendConfig.getInstance().getMaxLoadUsers())).andReturn(keyCloakUsersList);
        expect(JwtHelper.getRolesForUser(jwt, keyCloakUsersList.get(0).getId())).andReturn(keyCloakRolesList);

        PowerMock.replayAll();
        task.run();
        PowerMock.verifyAll();

        List<KeyCloakUser> keyCloakUsersResult = UserCache.getInstance().getKeyCloakUsers();

        assertEquals(1, keyCloakUsersResult.size());
        assertEquals(18, keyCloakUsersResult.get(0).getAllRoles().size());
        assertEquals("feedin-management-access", keyCloakUsersResult.get(0).getAllRoles().get(0));
        assertEquals("testclientrole", keyCloakUsersResult.get(0).getAllRoles().get(17));
    }

    @Test
    public void testGetKeyCloakUsers_EmptyToken() throws Exception {
        PowerMock.mockStatic(JwtHelper.class);
        List<KeyCloakUser> compList = new ArrayList<>();
        UserCache.getInstance().setKeyCloakUsers(compList);

        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles.json");
        Type listTypeKeyCloakRole = new TypeToken<List<KeyCloakRole>>(){}.getType();
        List<KeyCloakRole> keyCloakRolesList = JsonGeneratorBase.getGson().fromJson(jsonKeyCloakRoles, listTypeKeyCloakRole);

        expect(JwtHelper.login(BackendConfig.getInstance().getKeycloakAdmin(), BackendConfig.getInstance().getKeycloakPW())).andReturn(null);

        PowerMock.replayAll();
        task.run();
        PowerMock.verifyAll();

        assertTrue(UserCache.getInstance().getKeyCloakUsers().isEmpty());
    }

    @Test
    public void testGetKeyCloakUsers_null() throws Exception {
        PowerMock.mockStatic(JwtHelper.class);
        List<KeyCloakUser> compList = new ArrayList<>();
        UserCache.getInstance().setKeyCloakUsers(compList);

        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles.json");
        Type listTypeKeyCloakRole = new TypeToken<List<KeyCloakRole>>(){}.getType();
        List<KeyCloakRole> keyCloakRolesList = JsonGeneratorBase.getGson().fromJson(jsonKeyCloakRoles, listTypeKeyCloakRole);

        expect(JwtHelper.login(BackendConfig.getInstance().getKeycloakAdmin(), BackendConfig.getInstance().getKeycloakPW())).andReturn(null);

        PowerMock.replayAll();
        task.run();
        PowerMock.verifyAll();

        assertTrue(UserCache.getInstance().getKeyCloakUsers().isEmpty());
    }


    @Test
    public void testGetKeyCloakUsersFromToken_ListEmpty() throws Exception {
        PowerMock.mockStatic(JwtHelper.class);
        String jsonKeyCloakUser = super.loadStringFromResource("testKeyCloakUsers.json");
        Type listTypeKeyCloakUser = new TypeToken<List<KeyCloakUser>>(){}.getType();
        List<KeyCloakUser> keyCloakUsersListOrg = JsonGeneratorBase.getGson().fromJson(jsonKeyCloakUser, listTypeKeyCloakUser);

        UserCache.getInstance().setKeyCloakUsers(keyCloakUsersListOrg);
        String json = super.loadStringFromResource("JWTAdmin.json");
        JwtToken jwt = getGson().fromJson(json, JwtToken.class);

        List<KeyCloakUser> keyCloakUsersList = new ArrayList<>();

        expect(JwtHelper.login(BackendConfig.getInstance().getKeycloakAdmin(), BackendConfig.getInstance().getKeycloakPW())).andReturn(jwt);
        expect(JwtHelper.getUsers(jwt, BackendConfig.getInstance().getMaxLoadUsers())).andReturn(keyCloakUsersList);

        PowerMock.replayAll();
        task.run();
        PowerMock.verifyAll();

        assertEquals(keyCloakUsersList.size(), UserCache.getInstance().getKeyCloakUsers().size());
    }
}
