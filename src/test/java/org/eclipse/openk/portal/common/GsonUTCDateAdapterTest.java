/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.Calendar;

public class GsonUTCDateAdapterTest {

    @Test
    public void serializeTest() {
        GsonUTCDateAdapter gsonUTCDateAdapter = new GsonUTCDateAdapter();
        Calendar calendar = Calendar.getInstance();
        java.util.Date date = calendar.getTime();
        Type type = new Type() {
            @Override
            public String getTypeName() {
                return null;
            }
        };
        JsonSerializationContext jsonSerializationContext = new JsonSerializationContext() {
            @Override
            public JsonElement serialize(Object o) {
                return null;
            }

            @Override
            public JsonElement serialize(Object o, Type type) {
                return null;
            }
        };

        gsonUTCDateAdapter.serialize(date, type, jsonSerializationContext);
    }

}
