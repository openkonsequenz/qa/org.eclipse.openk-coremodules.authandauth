/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common.util;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Objects;
import org.junit.Test;

public class HashingAlgoTest {
	@Test
	public void testHashIt() {
		String hash = HashingAlgo.hashIt("TestMe");
		assertTrue("66d565a2eb74e1a20b6c12bb2cbf84ecaade84ec".equals(hash));
		assertTrue(!Objects.equals(HashingAlgo.hashIt("ThisIsTest"), HashingAlgo.hashIt("thisIsATest")));
		assertNull(HashingAlgo.hashIt(null));
	}
}
