/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import org.eclipse.openk.portal.auth2.model.KeyCloakUser;
import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.viewmodel.UserModule;
import org.eclipse.openk.portal.viewmodel.VersionInfo;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.easymock.EasyMock.expect;


public class ControllerImplementationsTest extends ResourceLoaderBase {
    private BackendController beMock;
    @Before
    public void prepareTests() {
        beMock = PowerMock.createNiceMock(BackendController.class);
    }

    @Test
    public void testGetUsers() throws PortalException {
        List<KeyCloakUser> userList = new ArrayList<>();
        ControllerImplementations.GetUsers controllerImpl = new ControllerImplementations.GetUsers();
        controllerImpl.setModUser("EgalUser");
        controllerImpl.setUserId(14);

        assertEquals(controllerImpl.invoke().getStatus(), Globals.HTTPSTATUS_OK);
        assertEquals(controllerImpl.getModUser(), "EgalUser");
        assertEquals(controllerImpl.getUserId(), 14);
    }

    @Test
    public void testGetVersionInfo() throws PortalException {
        VersionInfo vi = new VersionInfo();
        ControllerImplementations.GetVersionInfo controllerImpl = new ControllerImplementations.GetVersionInfo(beMock);

        expect(beMock.getVersionInfo()).andReturn(vi);
        PowerMock.replay(beMock);

        assertEquals(controllerImpl.invoke().getStatus(), Globals.HTTPSTATUS_OK);
    }

    @Test
    public void testLogOut() throws PortalException {
        ControllerImplementations.Logout controllerImpl = new ControllerImplementations.Logout("LogMeOut");
        assertEquals(controllerImpl.invoke().getStatus(), Globals.HTTPSTATUS_OK);
    }

    @Test
    public void testCheckAuth() throws PortalException {
        ControllerImplementations.CheckAuth controllerImpl = new ControllerImplementations.CheckAuth();
        assertEquals(controllerImpl.invoke().getStatus(), Globals.HTTPSTATUS_OK);
    }

    @Test
    public void testGetUserModulesForUser() throws PortalException {
        List<UserModule> userModules = new ArrayList<>();
        ControllerImplementations.GetUserModulesForUser controllerImpl = new ControllerImplementations.GetUserModulesForUser(beMock);
        controllerImpl.setModUser("EgalUser");

        expect(beMock.getUserModuleList()).andReturn(userModules);
        PowerMock.replay(beMock);

        assertEquals(controllerImpl.invoke().getStatus(), Globals.HTTPSTATUS_OK);
        assertEquals(controllerImpl.getModUser(), "EgalUser");
    }

    @Test
    public void testGetUserForRole() throws PortalException {
        List<KeyCloakUser> keyCloakUsers = new ArrayList<>();
        String userRole = "theBestRole";
        ControllerImplementations.GetUsersForRole controllerImpl = new ControllerImplementations.GetUsersForRole(beMock, userRole);
        controllerImpl.setModUser("EgalUser");

        expect(beMock.getUsersForRole(userRole)).andReturn(keyCloakUsers);
        PowerMock.replay(beMock);

        assertEquals(controllerImpl.invoke().getStatus(), Globals.HTTPSTATUS_OK);
        assertEquals(controllerImpl.getModUser(), "EgalUser");
    }
}
