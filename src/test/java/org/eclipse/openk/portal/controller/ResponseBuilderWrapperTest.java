/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;


import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Response;
import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.exceptions.PortalException;
import org.eclipse.openk.portal.exceptions.PortalInternalServerError;
import org.junit.Test;


public class ResponseBuilderWrapperTest {
    @Test
    public void testGetResponseBuilder() throws PortalInternalServerError {
        String json = "{ 'ret' : 'OK' }";
        Response.ResponseBuilder rb = ResponseBuilderWrapper.INSTANCE.getResponseBuilder( json );
        Response resp = rb.build();
        assertEquals(Globals.HTTPSTATUS_OK, resp.getStatus());
    }

    @Test
    public void testBuildOkResponse() throws PortalException {
        String json = "{ 'test' : 'Value' }";
        Response resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse( json );
        assertEquals( Globals.HTTPSTATUS_OK, resp.getStatus());

        resp = ResponseBuilderWrapper.INSTANCE.buildOKResponse(json, "ssess");
        assertEquals( Globals.HTTPSTATUS_OK, resp.getStatus());
    }
}
