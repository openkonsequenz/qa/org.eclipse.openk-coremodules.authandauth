/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.util;

import org.eclipse.openk.portal.auth2.model.JwtPayload;
import org.eclipse.openk.portal.auth2.model.JwtToken;
import org.eclipse.openk.portal.auth2.model.KeyCloakRole;
import org.eclipse.openk.portal.auth2.util.JwtHelper;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.exceptions.PortalInternalServerError;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertNotNull;

public class JwtHelperTest extends ResourceLoaderBase {

    @Test
    public void testGetJwtPayload_Base64URL() {
        //Token Pre 4.8.3 Keycloak
        String base64urlToken = "AA.eyJqdGkiOiI3NDVmOTc4Zi00OTYwLTRlO"+
                                "WYtYmFiMS1jNjdkZjMzODEwYjQiLCJleH"+
                                "AiOjE1MTYwMTAxNDksIm5iZiI6MCwiaWF"+
                                "0IjoxNTE2MDA5ODQ5LCJpc3MiOiJodHRw"+
                                "Oi8vbG9jYWxob3N0OjgwOTAvYXV0aC9yZ"+
                                "WFsbXMvTVZWTmV0emUiLCJhdWQiOiJlbG"+
                                "9nYm9vay1iYWNrZW5kIiwic3ViIjoiNTk"+
                                "5ZWY3NzMtNjRmYS00MzM2LWIwNDktMmEz"+
                                "Njc4NTEzZTU4IiwidHlwIjoiQmVhcmVyI"+
                                "iwiYXpwIjoiZWxvZ2Jvb2stYmFja2VuZC"+
                                "IsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9"+
                                "zdGF0ZSI6ImE2OGE4MzNkLTRiNTItNDgw"+
                                "My05MDU4LTRkNjJhNmI3NzI3MCIsImFjc"+
                                "iI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOl"+
                                "siKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9"+
                                "sZXMiOlsiZWxvZ2Jvb2stYWNjZXNzIiwi"+
                                "ZWxvZ2Jvb2stbm9ybWFsdXNlciIsInVtY"+
                                "V9hdXRob3JpemF0aW9uIl19LCJyZXNvdX"+
                                "JjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJ"+
                                "yb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIs"+
                                "Im1hbmFnZS1hY2NvdW50LWxpbmtzIiwid"+
                                "mlldy1wcm9maWxlIl19fSwicm9sZXMiOi"+
                                "JbdW1hX2F1dGhvcml6YXRpb24sIGVsb2d"+
                                "ib29rLWFjY2VzcywgZWxvZ2Jvb2stbm9y"+
                                "bWFsdXNlciwgb2ZmbGluZV9hY2Nlc3NdI"+
                                "iwibmFtZSI6IlV3ZSByb8OfIiwicHJlZm"+
                                "VycmVkX3VzZXJuYW1lIjoib3Blbmt0ZXN"+
                                "0IiwiZ2l2ZW5fbmFtZSI6IlV3ZSIsImZh"+
                                "bWlseV9uYW1lIjoicm_DnyJ9.CC";
        //Since Keycloak 4.8.3 Aud Parameter is an array now
        base64urlToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIRWQ4NGl4V2xaVWk2bWFSa005Tk1wSEd0amRXRU1pZGtNV0Nvbk90U1ZVIn0.eyJleHAiOjE2MTA3MzYxNDEsImlhdCI6MTYxMDczNTg0MSwianRpIjoiNGIwMTM1NGMtN2NmZC00ZmEyLThmMjQtYWM0ZmJlZWZhNTZkIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MTgwL2F1dGgvcmVhbG1zL0Vsb2dib29rIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6IjAzNDU0MjJmLTk0MzctNDVmYy1iMmY0LTNlYTcwZDUzY2Q3NCIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJzZXNzaW9uX3N0YXRlIjoiNDRlMWQxMTctYTU0My00NWZjLWJjNTAtZjE5MDYzZGVmOTk0IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJlbG9nYm9vay1ub3JtYWx1c2VyIiwicGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyIiwiZWxvZ2Jvb2stYWNjZXNzIiwiZWxvZ2Jvb2stc3VwZXJ1c2VyIiwia29uLWFkbWluIiwiZ3JpZC1mYWlsdXJlLWFjY2VzcyIsIm9mZmxpbmVfYWNjZXNzIiwia29uLWFjY2VzcyIsImdyaWQtZmFpbHVyZS1hZG1pbiIsInBsYW5uZWQtcG9saWNpZXMtYWNjZXNzIiwicGxhbm5pbmctYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJwbGFubmVkLXBvbGljaWVzLXN1cGVydXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InJlYWxtLW1hbmFnZW1lbnQiOnsicm9sZXMiOlsidmlldy11c2VycyIsInF1ZXJ5LWdyb3VwcyIsInF1ZXJ5LXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJBZG1pbiBTdHJhdG9yIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4iLCJnaXZlbl9uYW1lIjoiQWRtaW4iLCJmYW1pbHlfbmFtZSI6IlN0cmF0b3IifQ.sz5nOZZDwDHUS-WLzx1KX_KGTiWGuWTisR_bbHyReeXJyMefL6pngYwF_Qt0EPk1g0rqmqdhTN8-5IaSrwO1n0RmZrWLg19n1QnjkAs6ebF84UL07n5EEFJKh0luyWOKb_EW8JZpwRpLhaiWTG6KdXnOU6VkxJ-xX0ncmrHS8MYDh_u1LhIUPnu43trn0jJWoNAuHHVMY0rhpXNaZqc3Rq8iI3Vye87Yvs83jKTj5d1rHsFTrkaRt51bgi-2ML6IZa9GxIbCYroHZ7Idue00jisWPzmYU7F-wOpASBClBDsweTwRY9Dgr_yjrMns-1KXfYwtt4BTEc0IMKfX5gneSg";
        // this payload contains a char with is valid in Base64URL but NOT in Base64.
        JwtToken jt = new JwtToken();
        jt.setAccessToken(base64urlToken);
        JwtPayload pl = JwtHelper.getPayLoad(jt);
        assertNotNull( pl );

    }

    @Test
    public void testGetRolesListFromJson_testKeyCloakRoles_noClientRoleAtAll() throws PortalInternalServerError {
        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles_noClientRoleAtAll.json");
        List<KeyCloakRole> keyCloakRolesList = JwtHelper.getRolesListFromJson(jsonKeyCloakRoles);

        assertEquals("elogbook-access", keyCloakRolesList.get(0).getName());
        assertEquals(2, keyCloakRolesList.size());
    }

    @Test
    public void testGetRolesListFromJson_testKeyCloakRoles_noClientRoleForClient() throws PortalInternalServerError {
        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles_noClientRoleForClient.json");
        List<KeyCloakRole> keyCloakRolesList = JwtHelper.getRolesListFromJson(jsonKeyCloakRoles);

        assertEquals("elogbook-access", keyCloakRolesList.get(0).getName());
        assertEquals(2, keyCloakRolesList.size());
    }

    @Test
    public void testGetRolesListFromJson_testKeyCloakRoles_noRealmRole() throws PortalInternalServerError {
        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles_noRealmRole.json");
        List<KeyCloakRole> keyCloakRolesList = JwtHelper.getRolesListFromJson(jsonKeyCloakRoles);

        assertTrue(keyCloakRolesList.isEmpty());
    }

    @Test
    public void testGetRolesListFromJson_testKeyCloakRoles_onlyClientRole() throws PortalInternalServerError {
        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles_onlyClientRole.json");
        List<KeyCloakRole> keyCloakRolesList = JwtHelper.getRolesListFromJson(jsonKeyCloakRoles);

        assertEquals("view-profile", keyCloakRolesList.get(0).getName());
        assertTrue(keyCloakRolesList.get(0).getClientRole());
        assertEquals(2, keyCloakRolesList.size());
    }

    @Test
    public void testGetRolesListFromJson_testKeyCloakRoles_sameClientRealmRole() throws PortalInternalServerError {
        String jsonKeyCloakRoles = super.loadStringFromResource("testKeyCloakRoles_sameClientRealmRole.json");
        List<KeyCloakRole> keyCloakRolesList = JwtHelper.getRolesListFromJson(jsonKeyCloakRoles);

        assertEquals("elogbook-access", keyCloakRolesList.get(0).getName());
        assertEquals(4, keyCloakRolesList.size());
    }


}
