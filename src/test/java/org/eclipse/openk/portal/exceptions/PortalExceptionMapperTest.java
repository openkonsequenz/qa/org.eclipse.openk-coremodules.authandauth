/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.exceptions;


import static junit.framework.Assert.assertEquals;

import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.viewmodel.ErrorReturn;
import org.junit.Test;

public class PortalExceptionMapperTest extends ResourceLoaderBase {
    @Test
    public void testToJson() {
        String json = PortalExceptionMapper.toJson(new PortalNotFound("lalilu"));

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(er.getErrorCode(), 404);
        assertEquals("lalilu", er.getErrorText());
    }

    @Test
    public void testUnknownErrorToJson() {
        String json = PortalExceptionMapper.unknownErrorToJson();

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(er.getErrorCode(), 500);
    }

    @Test
    public void testGeneralOKJson() {
        String ok = PortalExceptionMapper.getGeneralOKJson();
        assertEquals("{\"ret\":\"OK\"}", ok);
    }

    @Test
    public void testGeneralErrorJson() {
        String nok = PortalExceptionMapper.getGeneralErrorJson();
        assertEquals("{\"ret\":\"NOK\"}", nok);
    }



}
