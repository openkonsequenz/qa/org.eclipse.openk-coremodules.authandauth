/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.exceptions;

import static org.junit.Assert.assertEquals;

import org.eclipse.openk.portal.common.Globals;
import org.junit.Test;

public class PortalExceptionsTest {

    @Test
    public void testConstructors() {
        assertEquals(Globals.HTTPSTATUS_BAD_REQUEST, new PortalBadRequest().getHttpStatus() );
        assertEquals(Globals.HTTPSTATUS_FORBIDDEN, new PortalForbidden().getHttpStatus());
        assertEquals(Globals.HTTPSTATUS_INTERNAL_SERVER_ERROR, new PortalInternalServerError(null, null).getHttpStatus());
        assertEquals(Globals.HTTPSTATUS_NOT_FOUND, new PortalNotFound().getHttpStatus());
        assertEquals(Globals.HTTPSTATUS_UNAUTHORIZED, new PortalUnauthorized().getHttpStatus());
    }


    @Test
    public void testConstructors2() {
        final String extext = "ExText";
        assertEquals(new PortalBadRequest(extext).getMessage(), extext);
        assertEquals(new PortalForbidden(extext).getMessage(), extext);
        assertEquals(new PortalInternalServerError(extext).getMessage(), extext);
        assertEquals(new PortalNotFound(extext).getMessage(), extext);
        assertEquals(new PortalUnauthorized(extext).getMessage(), extext);
    }
}
